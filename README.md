# Cliiq Beta front end

### Node server and client: SSR React 16.9+

This is the Cliiq Beta front end codebase.

## Setup

`yarn`

IMPORTANT: Manually install full-icu for locale dates in Node.js: `yarn add full-icu`

## Development

`yarn dev` (development server)

Open or refresh the browser at `http://localhost:5000`

#### Analyse javascript bundle (code + npm dependencies)

A data visualisation of the client bundle can be generated with: `yarn analyze` (only browser bundle, not server). Use this to identify packages that are not worth their weight.

_@TODO_ code split will be introduced at some point to further reduce the weight of downloads.

#### Kill

In case one of the development servers or the production server does not close correctly, `yarn kill` will kill them all.

## Continuous Integration

See [Front end deployments](conventions/frontend-deployments.md)

## Versioning

See [Front end versioning](conventions/frontend-versioning.md)

## Production builds

`yarn run build` builds the Node server and client bundles

`yarn start` builds everything and launches the production server, accessible at `http://localhost:5000` (PM2)

## Check updated dependencies

To check if there are newer packages: `npx npm-check-updates`
