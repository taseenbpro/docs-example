# Cliiq front end development conventions

- [Front end deployments](conventions/frontend-deployments.md)
- [Front end versioning](conventions/frontend-versioning.md)
- [NPM & dependencies](conventions/dependencies.md)
