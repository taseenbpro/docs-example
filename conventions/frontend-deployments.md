# Cliiq front end *manual* deployments


Front End repository: https://github.com/cliiqco/frontend


## Continuous Integration server

- The `ci` branch will contain micro updates (merged from development branches).

- The `ci` branch will be automatically deployed on Heroku: http://frontend-9fcf2c98fae6.herokuapp.com/


## QA and Production servers process

- The `ci` branch can be merged to `master` only when stable.

- The `master` branch is supposed to be always ready for deployment to the QA server.

- When QA is ready for production, upgrade the version in `package.json` (see [Versioning](frontend-versioning.md)).

- Create a new release/tag from `master` with the same version.

- Deploy in production.


## Summary

1- The `ci` branch will only be used in Heroku.

2- The `master` branch can be ALWAYS deployed in QA.

3- The `master` branch can ONLY be deployed in production when a new release/tag is created.
