# Cliiq front end versioning

Cliiq Beta starts at `v0.0.1`.

- `Patch version`: Normal updates and bug fixes.

- `Minor version`: Fundamental changes involving multiple new features or improvements.

For the entire duration of Cliiq Beta we will keep the major version at `0` and only the patch and minor versions will change.

We will move to version `v1.0.0` only when the public Beta stage will be over (removal of "Beta" from the name and logo).
