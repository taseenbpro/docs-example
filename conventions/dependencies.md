### NPM and dependencies

We aim to keep the number of npm dependencies as low as possible. To keep a maintenable and easily updatable codebase we ask all developers to respect these rules when deciding to use dependencies:

- **Only** add npm dependencies that are indispensable, have reputable developers or belong/relate to industry standard packages, are currently maintained (less than 1 year from last release) or aren't trivial to reproduce internally.

- **Prefer** to copy and edit/simplify the code of small (single file) npm packages. Mostly when they are very simple or only a tiny part of them is actually needed.

- **Never** add large npm dependencies/libraries that would add considerable weight to the browser bundle and only relate to a very specific functionality. In those cases, add it as a static asset and load it asynchronously in the code using the `loadScript` helper function.

- Note that there are folders for shared React components (`/src/universal/components/shared/`), helper functions (`/src/universal/helpers/`) and higher order components (`/src/universal/HOCs/`). You might find or add what you need there in many cases.