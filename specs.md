# Cliiq components definition (alpha)


## Core components

- [Story](specs/story.md)
- [Comments](specs/comments.md)
- [Search](specs/search.md)
- [User](specs/user.md)

## Specific components

- [Image formats](specs/image-formats.md)
- [URL to widget](specs/url-to-widget.md)
- [Validations and strings](specs/validations-and-strings.md)
- [Profile image upload](specs/profile-images-upload.md)
