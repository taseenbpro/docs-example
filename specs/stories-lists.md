# Stories lists

Details about the lists of stories (posts) used in Cliiq. We need to show these lists in several situations:

* Home => ***Latest stories***
* Profile => ***User stories***
* Profile => ***Likes***
* Search => ***Stories results***

These lists will normally appear in modal windows or in a dedicated page. In all cases we will need pagination information.

The list is an object with 2 keys:

```
{
  meta: {
     totalItems: 32,
     itemsPerPage: 15,
     currentPage: 0
  },
  data: [
    {...post},
    {...post},
    {...post}
  ]
}

```