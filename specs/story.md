# Story

Details about the Story component in Cliiq.


### Principles

* Once created the story is a draft (`published` property is `false`).
* The story content is a represented by a list of *sections* of different types.
* The story must contain at least one Section of type Image to be valid.
* The story must have only one Image with `isLead` set to `true`. If not the first image available should be used as lead image.
* The properties `title` and `intro` are not mandatory. The user can create a story made of images only.


### Permalink

* Each story has a unique ID that will be used to identify its url: `https://cliiq.co/s/uuid-here` 


* **DEPRECATED** The story has a unique "name" that will be used to create the url: `https://cliiq.co/@username/story` **DEPRECATED**


### Data structure

The Story is represented by an object with 2 elements: `meta` (object) and `data` (array).


## Meta

The `meta` object contains the following (M for mandatory):

* `id` *{string}* M Unique id
* `seoName` *{**DEPRECATED**}* M SEO friendly name of the story (with spaces and special characters stripped, used to create the permalink)
* `name` *{**DEPRECATED**}* M The name
* `title` *{string}* The first heading section of the story
* `intro` *{string}* Truncated copy from the first text section of the story
* `leadImage` *{string}* M Image url
* `createdAt` *{string}* M Date
* `updatedAt` *{string}* M Date
* `author` *{object}* M User object
* `published` *{boolean}* M If the story is public or in draft mode
* `stats` *{object}* The story Stats object (see Story Stats => TO DO)
* `comments` *{array}* List of comment objects (see Comment => TO DO)


## Data

The Story is simply a list of *sections* (Section component). So the `data` is an array that contains the sections. 
Each Section is represented by an object of one of the following types:

* Heading
* Text
* Quote
* Break
* Image
* Media

All sections must contain some common key/value pairs:

* `id` *{string}* M Unique id of the section
* `type` *{string}* M The type of section (lowercase)

#### Heading

The Heading represents a title or heading text (simple text).

* `value` *{string}* Copy of the heading


#### Text

The Text represents a paragraph (simple text).

* `value` *{string}* M Copy of the paragraph


#### Quote

The Quote represents a paragraph (simple text).

* `value` *{string}* M Copy of the quote


#### Break

The Break respresent a visual break between sections of content. It has no specific key/value.


#### Image

The Image represents... guess what?

* `src` *{string}* M Image url
* `caption` *{string}* Copy for the image caption
* `format` *{number}* M The maximum width:
    * 0 = normal (same as paragraph)
    * 1 = large (page container)
    * 2 = full bleed (window width)
* `width` *{number}* M Width in pixels
* `height` *{number}* M Height in pixels
* `isLead` {boolean} Whether the image must be used in the lists.
* `tags` *{array}* List of Tag objects

#### Media

The Media represents links to video or audio provider (and possibly any link in the future, like tweets, maps or any website page).

* `provider` *{string}* M Media provider (example: 'youtube', 'vimeo', 'soundcloud', etc)
* `opts` *{object}* M Media provider optional details (can contain any amount and type of key/value pairs)

In most cases (YouTube, Vimeo) `opts` will simply contain `mediaId` and `url`. But in some cases we will have more details and params (example: `params` object containing provider specific parameters like `start` time or a `mediaType` containing a string `playlist` or `track`).

