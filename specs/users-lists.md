# Users lists

Details about the lists of users used in Cliiq. We need to show these lists in several situations:

* Profile => ***Followers***
* Profile => ***Following***
* Story => ***Cliiqs***
* Story => ***Likes***
* Settings => ***Blocked users***
* Search => ***People results***

These lists will normally appear in modal windows or in a dedicated page. In all cases we will need pagination information.
