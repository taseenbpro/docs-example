# Image formats

Details about image management in Cliiq.

## Metadata

Keep metadata in the edited image files.


## Max upload size

Maximum file size: 10 MB


## Avatar

Min upload size: 384x384

Crop/resize to square (based on front end request).

Versions (6):

- 36x36 (nav button)
- 72x72 (nav button hdpi)
- 48x48 (post author)
- 96x96 (post author hdpi)
- 180x180 (profile)
- 360x360 (profile hdpi)


## Profile head image

Min upload size 780x260.

Resize/crop to 3:1 ratio (based on front end request).

Versions (3):

- 290x90 (card)
- 780x260 (small device)
- 1500x500


## Images inside a story

No min width/height, never crop.


## Story lead image

No min size, never crop.

Versions (2):

- max width: 740 (lists: profile, home, search)
- max width 2500
