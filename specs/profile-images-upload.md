# User profile images: upload and edit process

The user must be able to upload images for their avatar and head image.

### Features

- The user can select a single image from their device, by entering the "edit mode".
- To be valid the image must be equal or larger than the minimum width/height (client validation).
- If the image is larger than the container, the user can resize and position it.
- The user can confirm/save so that the image will be uploaded and treated in the backend.

### Resize and position

- The image selected by the user is initially centered (vertically and horizontally) and resized to fill the container (browser).
- Once confirmed by the user, the image is uploaded along with some data:
  - Scale factor or width/height (TBD)
  - Crop coordinates (top/left corner, width/height)
- The server should then:
  - Scale the image
  - Crop the image
  - Save the image
  - Return the profile details with the updated URL of the image
