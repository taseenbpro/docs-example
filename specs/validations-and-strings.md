# Validations and special strings

Treatment of special content in strings.


### Usernames and mentions

- Valid usernames: 3 to 16 characters.
- Mentions are references to users and they start with `@`.
- Mentions get parsed in the browser and converted into links pointing to the user profile.
- Mentions longer than 16 characters are invalid and don't get converted into links.
- Valid mentions always count as 16 characters, regardless their actual length.


### Hashtags

- Valid hashtags: 1 to 50 characters (excluding `#`).
- Hashtags are keywords used to categorise a content by topic and start with `#`.
- Hashtags get parsed in the browser and converted into links pointing to search results.
- A hashtag containing invalid characters will be valid until the first invalid character. Example: `#it'sfun` will be interpreted as the valid hashtag `#it` and a normal string `'sfun`.
- Hashtags characters must be counted normally: `#HashtagHashtagHashtagHashtagHashtagHashtagHashtag` is 50 characters.
- An invalid hashtag (longer than 50 characters or starting with an invalid character) will not be parsed (browser only).


### Validation of user details

#### Username

- Mandatory.
- Minimum 3 characters.
- Maximum 16 characters.
- Only letters, numbers, underscore
- Allowed: letters (upper/lowercase), numbers, underscore.


#### Email

- Mandatory.
- Only valid and verified email.


#### Name

- Mandatory.
- Minimum 1 character.
- Maximum 50 characters.
- Allowed: letters and accented (upper/lowercase), numbers, underscore, emojis.
- Parsed (browser only): emojis.


#### Telephone

- Optional.
- Only valid and verified telephone.
- Validation through: 
  - Server: https://github.com/google/libphonenumber
  - Browser: https://github.com/catamphetamine/libphonenumber-js


#### Bio

- Optional.
- Up to 200 characters.
- Allowed: any character.
- Parsed (browser only): urls, emails, hashtags, mentions, emojis, other unicode encoded characters.


#### Website

- Optional.
- Only valid urls starting with `http` or `https`.


#### Location

- Optional.
- Allowed: anything needed to define a location from names to coordinates.
- Up to 75 characters. (?)
