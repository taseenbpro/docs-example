# Comments

Comments will play a fundamental role in Cliiq and will be one of the main ways people will interact with the platform. Therefore we intend to enhance their features and better define their rules.

### Limits

Unlike stories, comments will be subject to strict limitations and specific validation checks (more or less like tweets):

- Min 1 character.
- Max 250 characters.
- Max 1 image OR 1 media element (video or audio) OR visualised link (open graph, TBD).


### Reactions

Users can react to comments with like, share and/or other comments (creating a thread).


### Feed

Comments in the feed can have a *full* or *short* format.

#### New comments
New comments will be presented in the feed in *full* format. The design is not done yet, but the idea is that *new* comments will appear as prominent, compared to the story they belong to.

#### Comments in a shared or newly published story
Stories in the feed might show 1 or 2 comments in *short* format. In this case the story will be prominent.

#### Enhanced feed: when stories or comments start to have traction
In the context of a more general improvement of the feed, we should consider that if a comment (or story) has a particularly high number of reactions, it should also appear in the feed with a special mention like "Trending" (regardless its publication date).
