# Open Graph and Schema integration

### URL to widget for Cliiq

Set of tools to convert a url into a visual representation in a Cliiq story or comment, based on Open Graph and Schema.org data.


## OG scraper (back end)

Backend service capable of extracting Open Graph and other meta tags from any public URL.

The API endpoint could be `GET /og?url={url}` and would return an object containing:
- Open Graph fields
- Twitter Card fields
- Any other meta tag
- Page title

#### OG types

The meta tag `<meta property="og:type" content="website" />` helps identify the type of content. 

The most used types are `website` (default), `article`, `profile`, `book`, `video.movie`, `music.song`, etc.

Apart from the generic `website`, our main interest at the moment is focused on:
- `product` (ex: used by Shopify, bigcartel.com, etc)
- `event` (ex: used by Eventbrite)


## Schema scraper (back end)

Schema.org is a complementary solution, maybe even better. This is used by websites of all sizes but also by many big names (Shopify, Ebay, Samsung, Apple, Intel, Nytimes, Microsoft, Ikea, etc). This standard adds an attribute `itemprop` to actual HTML elements (instead of hidden meta tags). It has models for almost anything: Product, Event, Place, Person, Organization, Movie, Recipe, Review, etc.

In the case of `Product` (https://schema.org/Product) and depending on the authors of the website, we can get all sorts of fields like: name, price, priceCurrency, category, image, color, similarTo, relatedTo, logo, manufacturer, material, model, etc.

```
// Ebay example

<div id="my-product" itemscope="itemscope" itemtype="https://schema.org/Product">
  <h1 itemprop="name">Gnome Sculpture</h1>
  <span itemprop="price" content="24.99">2£4.99 each</span>
  <span itemprop="priceCurrency" content="GBP"></span>
  <img itemprop="image" src="https://i.ebayimg.com/images/g/...jpg" />
</div>
```

In this case the API endpoint could be `GET /schema?url={url}` and would return an array containing all elements with an `itemscope` and `itemtype` attribute found, as objects. For example:

```
// From the Ebay example

[
  {
    itemtype: "https://schema.org/Product",
    name: "Gnome Sculpture",
    price: 24.99,
    priceCurrency: "GBP",
    image: "https://i.ebayimg.com/images/g/...jpg"
  }
]
```

More info: https://schema.org/docs/schemas.html


## Combination of OG and Schema

Given a URL the server returns all OG and Schema data found.

For example: `GET /scrap?url={url}`


## Image hosting or iframe (security)

`og:image` (or the image of a Schema object) should be the url of an image, but it could return anything.

Because of the unreliable nature of external resources (server up/down, connection speed, security) it could be useful to host a copy of the image found in `og:image`.

If this is too costly, we can use a front end component designed to load external images in a sandboxed iframe (that would stop any content to get access to the front end application and the rest of the page).


## Cliiq support (front end)

We should initially support OG `website`, `product`, `article` and Schema Product. We could later add support for other types like Event, Place.
