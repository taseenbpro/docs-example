# User

Details about the User component in Cliiq.


### Data structure

The User is represented by an object:

- username {string}
- name: {string}
- first_name: [DEPRECATED]
- last_name: [DEPRECATED]
- avatar_url {string or undefined} 
- head_image_url {string or undefined} 
- biography {string}
- website {string}
- followers_count {number}
- following_count {number}
- likes_count {number}
- profile_verified {boolean}
- followed {boolean or undefined} Whether the user follows @me
- follower {boolean or undefined} Whether @me follows the user
- posts {array} First page of stories list object
