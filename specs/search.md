# Search Results

Details about the Search Results component in Cliiq. This component represents the response to a search request, based on a string sent from the client (`query`).


### Data structure

The Search Results represents an object with 2 elements: `meta` (object) and `data` (array).

## Meta

The `meta` object contains the following:

* `query` *{string}* The requested search string sent from the client

[DEPRECATED * `type` *{string}* The type of data. It can be:]: # 
[DEPRECATED     * `story` A list of post objects]: #     
[DEPRECATED     * `user` A list of user objects ]: #     

It also contains standard pagination details:

* `totalItems` *{number}* The total items
* `itemsPerPage` *{number}* Amount of items per page sent from the server 
* `currentPage` *{number}* The current page
* `previousPage` *{number or null}* The previous page, if available
* `nextPage` *{number or null}* The next page, if available

## Data

The Search Results data is an object that contains 2 key/value pairs:

* `users` *{array}* A list of user objects
* `stories` *{array}* A list of story objects


Refer to `Story` and `User` docs for details on their structure.
